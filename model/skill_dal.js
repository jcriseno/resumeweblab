var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view skill_view as
 select s.*, a.street, a.zip_code from skill s
 join skill a on a.skill_id = s.skill_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM skill';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(skill_id, callback) {
    var query = 'SELECT * FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE skill
    var query = 'INSERT INTO skill (skill_name, description) VALUES (?,?)';

    var queryData = ([params.skill_name, params.description]);


        // NOTE THE EXTRA [] AROUND skillskillData
        connection.query(query, queryData, function(err, result){
            callback(err, result);
        });
//    });

};

exports.delete = function(skill_id, callback) {
    var query = 'DELETE FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var skillskillInsert = function(skill_id, skillIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO skill_skill (skill_id, skill_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var skillskillData = [];
    if (skillIdArray.constructor === Array) {
        for (var i = 0; i < params.skill_id.length; i++) {
            skillskillData.push([skill_id, params.skill_id[i]]);
        }
    }
    else {
        skillskillData.push([skill_id, params.skill_id]);
    }
    connection.query(query, [skillskillData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.skillskillInsert = skillskillInsert;

//declare the function so it can be used locally
var skillskillDeleteAll = function(skill_id, callback){
    var query = 'DELETE FROM skill_skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.skillskillDeleteAll = skillskillDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE skill SET skill_name = ?, description = ? WHERE skill_id = ?';
    var queryData = [params.skill_name, params.description, params.skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
        /*
        //delete skill_skill entries for this skill
        skillskillDeleteAll(params.skill_id, function(err, result){

            if(params.skill_id != null) {
                //insert skill_skill ids
                skillskillInsert(params.skill_id, params.skill_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });
        */
    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS skill_getinfo;

     DELIMITER //
     CREATE PROCEDURE skill_getinfo (skill_id int)
     BEGIN

     SELECT * FROM skill WHERE skill_id = _skill_id;

     SELECT a.*, s.skill_id FROM skill a
     LEFT JOIN skill_skill s on s.skill_id = a.skill_id AND skill_id = _skill_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL skill_getinfo (4);

 */

exports.edit = function(skill_id, callback) {
    var query = 'SELECT * FROM skill WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};