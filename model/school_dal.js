var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view school_view as
 select s.*, a.street, a.zip_code from school s
 join school a on a.school_id = s.school_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * from school';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(school_id, callback) {
    var query = 'SELECT s.*, a.street, a.zip_code FROM school s ' +
        'LEFT JOIN address a on a.address_id = s.address_id ' +
        //'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE s.school_id = ?';
    var queryData = [school_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE school
    var query = 'INSERT INTO school (school_name, address_id) VALUES (?,?)';

    var queryData = [params.school_name, params.address_id];

    connection.query(query, queryData, function(err, result) {

        // THEN USE THE school_ID RETURNED AS insertId AND THE SELECTED school_IDs INTO school_school
        var school_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO school_address (school_id, address_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var schoolAddressData = [];
      //  if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.address_id.length; i++) {
                schoolAddressData.push([school_id, params.address_id[i]]);
            }
     //   }
       // else {
       //     schoolschoolData.push([school_id, params.school_id]);
       // }

        // NOTE THE EXTRA [] AROUND schoolschoolData
        connection.query(query, [schoolAddressData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(school_id, callback) {
    var query = 'DELETE FROM school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var schoolschoolInsert = function(school_id, schoolIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO school_address (school_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var schoolschoolData = [];
        for (var i = 0; i < schoolIdArray.length; i++) {
            schoolschoolData.push([school_id, schoolIdArray[i]]);
        }
    connection.query(query, [schoolschoolData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolschoolInsert = schoolschoolInsert;

//declare the function so it can be used locally
var schoolschoolDeleteAll = function(school_id, callback){
    var query = 'DELETE FROM school_school WHERE school_id = ?';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.schoolschoolDeleteAll = schoolschoolDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE school SET school_name = ? WHERE school_id = ?';
    var queryData = [params.school_name, params.school_id];

    connection.query(query, queryData, function(err, result) {

        //delete school_school entries for this school
        schoolschoolDeleteAll(params.school_id, function(err, result){

            if(params.school_id != null) {
                //insert school_school ids
                schoolschoolInsert(params.school_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS school_getinfo;

     DELIMITER //
     CREATE PROCEDURE school_getinfo (school_id int)
     BEGIN

     SELECT * FROM school WHERE school_id = _school_id;

     SELECT a.*, s.school_id FROM school a
     LEFT JOIN school_school s on s.school_id = a.school_id AND school_id = _school_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL school_getinfo (4);

 */

exports.edit = function(school_id, callback) {
    var query = 'CALL school_getinfo(?)';
    var queryData = [school_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};